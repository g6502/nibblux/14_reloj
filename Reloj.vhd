library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity reloj is 
	port(
		clk	: in	std_logic;
		BCD	: out	std_logic_vector(23 downto 0)
	);
	
end entity;

architecture behave of reloj is

	signal	freq_1Hz	: std_logic;
	signal	cambio	: std_logic;
	signal	T_sec	: std_logic_vector(7 downto 0)	:= x"32";
	signal	T_min	: std_logic_vector(7 downto 0)	:= x"3B";
	signal	T_hou	: std_logic_vector(7 downto 0)	:= x"17";
	
	
begin

	u0	:	entity work.div_freq
		generic map(
			fin_cont => 25_000_000)
		port map(
			clk => clk,
			dclk => freq_1Hz);
	
	u1	:	entity work.SHADD3
		port map(
			num => T_sec,
			BCD(7 downto 0) => BCD(7 downto 0));
	
	u2	:	entity work.SHADD3
		port map(
			num => T_min,
			BCD(7 downto 0) => BCD(15 downto 8));
	
	u3	:	entity work.SHADD3
		port map(
			num => T_hou,
			BCD(7 downto 0) => BCD(23 downto 16));
	
	process (clk)
		variable cont : integer range 0 to 50e6 := 0;
	begin
		if clk'event and clk = '1' then
			if cont = 50e6 - 1 then
				cambio <= '1';
				cont := 0;
			else
				cambio <= '0';
				cont := cont + 1;
			end if;
		end if;
	end process;
	
	process(clk,cambio)
	begin
		
		if rising_edge(clk) then
			--if	T_sec = 60 then
			if cambio = '1' then
				T_sec <= T_sec + 1;
			else
				if	T_sec = "00111100" then
					T_sec <= (others => '0');
					T_min <= T_min + 1;
				end if;
				
				--if	T_min = 60 then
				if	T_min = "00111100" then
					T_min <= (others => '0');
					T_hou <= T_hou + 1;
				end if;
				
				--if	T_hou = 24 then
				if	T_hou = "00011000" then
					T_hou <= (others => '0');
				end if;
			end if;
		end if;
	end process;
	
end behave;