library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
--use ieee.numeric_std.all;

entity SHADD3 is 
	port(
		num	: in	std_logic_vector(7 downto 0);
		BCD	: out	std_logic_vector(11 downto 0)
	);
end SHADD3;

architecture behavioral of SHADD3 is

begin
	process(num)
		variable aux	: std_logic_vector(19 downto 0);
	begin
		aux := (others => '0');
		aux(7 downto 0) := num;
		
		for i in 0 to 7 loop
			if aux(11 downto 8) > 4 then
				aux(11 downto 8) := aux(11 downto 8) + 3;
				end if;
			if aux(15 downto 12) > 4 then
				aux(15 downto 12) := aux(15 downto 12) + 3;
				end if;
			if aux(19 downto 16) > 4 then
				aux(19 downto 16) := aux(19 downto 16) + 3;
				end if;
			--aux := shift_left(unsigned(aux),1);
			aux(19 downto 1) := aux(18 downto 0);
		end loop;
		bcd <= aux(19 downto 8);
	end process;
end behavioral;